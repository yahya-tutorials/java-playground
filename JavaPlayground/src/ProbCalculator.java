import java.math.BigInteger;

public class ProbCalculator
{

    int ColludingCount;
    int SystemSize;
    int Alpha;
    BigInteger Negligible;

    public ProbCalculator(int colludingCount, int systemSize, int alpha, BigInteger negligible)
    {
        ColludingCount = colludingCount;
        SystemSize = systemSize;
        Alpha = alpha;
        Negligible = negligible;
    }

    public static BigInteger choose(int x, int y)
    {
        if (y < 0 || y > x)
            return BigInteger.ZERO;
        if (y == 0 || y == x)
            return BigInteger.ONE;

        BigInteger answer = BigInteger.ONE;
        for (int i = x - y + 1; i <= x; i++)
        {
            answer = answer.multiply(BigInteger.valueOf(i));
        }
        for (int j = 1; j <= y; j++)
        {
            answer = answer.divide(BigInteger.valueOf(j));
        }
        return answer;
    }

    public int findNegligibleThreshold()
    {
        BigInteger prob = new BigInteger("0");
        do
        {

            for(int i = 0; i <= Alpha; i++)
            {
                if((2 * Alpha > SystemSize) || Alpha + i > ColludingCount || Alpha - i > SystemSize - ColludingCount)
                {
                    System.out.println("No solution for " + SystemSize + " Alpha " + Alpha);
                    return Alpha;
                }
                prob = prob.add(choose(SystemSize, 2 * Alpha).divide(choose(ColludingCount, Alpha + i).multiply(choose(SystemSize - ColludingCount, Alpha - i))));
            }
            Alpha++;
        } while (prob.compareTo(Negligible) < 1);

        System.out.println("Number of required validators for " + SystemSize + " nodes is " + Alpha + " prob " + prob.toString());
        return --Alpha;
    }
}