import java.io.BufferedReader;
import java.io.FileReader;
import java.math.BigInteger;
import java.util.ArrayList;

public class Main
{

    public static void main(String[] args)
    {
        BigInteger negl = new BigInteger("10");
        negl = negl.pow(80);
        System.out.println("Negl = " + negl.toString());
        int alpha = 1;
        for(int size = 100; size <= Math.pow(10, 9); size*=10)
        {
            ProbCalculator pc  = new ProbCalculator((int) Math.ceil(0.16 * size), size, alpha, negl);
            alpha = pc.findNegligibleThreshold();
        }

    }


}
